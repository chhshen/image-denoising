#include <functional>
#include <utility>
#include <vector>

#include "caffe/loss_layers.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void PSNRLayer<Dtype>::LayerSetUp( const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) { }

template <typename Dtype>
void PSNRLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
	top[0]->Reshape(1, 1, 1, 1);
}

template <typename Dtype>
void PSNRLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {

  const int num_data = bottom[0]->count();
  const Dtype* bottom_data = bottom[0]->cpu_data();
  const Dtype* bottom_label = bottom[1]->cpu_data();

  Dtype MSE = 0;

  for (int i = 0; i < num_data; ++i) {
      double pixel_1 = static_cast<double>(bottom_data[i]) * 255;
      double pixel_2 = static_cast<double>(bottom_label[i]) * 255;
      MSE += (pixel_1 - pixel_2) * (pixel_1 - pixel_2);
  }

  MSE /= num_data;

  Dtype PSNR = log10(255 * 255 / MSE) * 10;

  top[0]->mutable_cpu_data()[0] = PSNR;
}

INSTANTIATE_CLASS(PSNRLayer);
REGISTER_LAYER_CLASS(PSNR);

}  // namespace caffe
